package com.sma.spring.project.test.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.qcst.jdbcmanager.JDBCManager;
import com.sma.spring.project.test.dao.WorkProDao;
import com.sma.spring.project.test.model.faq;

@Service
public class WorkProService implements WorkProDao {

	@Override
	public int insert(faq work) {
		return JDBCManager.insert("test", work);
	}

	@Override
	public int update(faq work) {
		return JDBCManager.update("test", work, "status='active'");
	}

	@Override
	public void delete(faq work) {
		JDBCManager.DeleteObj("test", work, "status='inactive'");
	}

	@Override
	public List<faq> select(faq work) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		return JDBCManager.selectBy("test", work, "status='active'");
	}

	@Override
	public List<faq> selectall(faq work) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		return JDBCManager.selectAll("test", work);
	}
	
	@Override
	public faq selectById(Long id) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		return JDBCManager.selectById("test",id, "id", new faq());
	}
	
}
