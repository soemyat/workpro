package com.sma.spring.project.test.model;

import java.util.Date;

public class faq {

	public static Long id;
	private String questions;
	private String answers;
	public static String status;
	public static Date createddate;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		faq.id = id;
	}

	public String getQuestions() {
		return questions;
	}

	public void setQuestions(String questions) {
		this.questions = questions;
	}

	public String getAnswers() {
		return answers;
	}

	public void setAnswers(String answers) {
		this.answers = answers;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		faq.status = status;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		faq.createddate = createddate;
	}
	
	@Override
	public String toString() {
		return "faq [id=" + id + ", questions=" + questions + ", answers=" + answers + ", status=" + status
				+ ", createddate=" + createddate + "]";
	}

}
