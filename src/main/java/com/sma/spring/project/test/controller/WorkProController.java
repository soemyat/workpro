package com.sma.spring.project.test.controller;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qcst.jdbcmanager.JDBCManager;
import com.sma.spring.project.test.dao.WorkProDao;
import com.sma.spring.project.test.model.faq;

@RestController
@RequestMapping(value="/faq")
public class WorkProController {

	@Autowired
	private WorkProDao dao;
	
	@GetMapping("/test")
	public String test()
	{
		return "gg";
	}
	
	@PostMapping("/save")
	public int saveFaq(@RequestBody faq wo)
	{	
		faq.status="active";
		faq.createddate=new Date();
		return dao.insert(wo);
	}
	
	@PostMapping("/update")
	public int updateFaq(@RequestBody faq wo)
	{
		return dao.update(wo);
	}
	
	@GetMapping("/delete")
	public void deleteFaq(faq wo)
	{
		dao.delete(wo);
	}
	
	@PostMapping("/selectbyid/{id}")
	public int selectByid(@PathVariable Long id, @RequestBody faq wo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException
	{
		faq fa = dao.selectById(id);
		fa.setQuestions(wo.getQuestions());
		fa.setAnswers(wo.getAnswers());
		fa.setStatus("inactive");
		return dao.update(fa);
	}
	
	@GetMapping("/select")
	public List<faq> selectFaq(faq wo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException
	{
		return dao.select(wo);
	}
	
	@GetMapping("/selectall")
	public List<faq> selectAllFaq(faq wo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException
	{
		return dao.selectall(wo);
	}
}
