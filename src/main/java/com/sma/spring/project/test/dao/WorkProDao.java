package com.sma.spring.project.test.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.sma.spring.project.test.model.faq;

public interface WorkProDao {

	int insert(faq work);
	int update(faq work);
	void delete(faq work);
	faq selectById(Long id) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException;
	List<faq> select(faq work) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException;
	List<faq> selectall(faq work) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException;

}
