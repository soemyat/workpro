package com.sma.spring.project.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkProApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkProApplication.class, args);
	}

}
